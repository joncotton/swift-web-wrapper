# Step by Step Guide to building a new App

## Prerequisites

- Mac OS X 10.10 (Yosemite)
- Xcode 6.3 (Latest version is available in the app store)

## Glossary

- **Target**: A product of the build or compilation, an application will have at least one target, usually a binary/executable file, for example, word.exe is the main target of the Microsoft Word application. Other things such as assets or possibly other executables (if you are building for multiple architectures) can be targets too.

- **Groups**: What Xcode projects call folders, however the groups within Xcode do not correlate to directories on your filesystem, they are just there to help you organise files within your project.

## Steps

1. Open Xcode, select new project, then choose iOS in the left hand menu and select 'Single View Application' as your template.

2. Fill in the app name and other details (note: The name is not the display name to the user, so please make a machine friendly name with no spaces). Make sure you select Swift as the language and leave the Devices as Universal (unless you know now that you're creating an app to specifically target only one device type). When asked about source control, you can either point to an existing repo or just select a git repo on this mac which will do a `git init` for you.

3. Save the project (just select the parent folder, Xcode will create the project folder for you).

4. Grab a terminal and cd to your new project dir and make a new lib dir `mkdir lib`

5. Now clone the JonKit project as a git submodule into the lib dir `git submodule add git@gitlab.com:joncotton/jonkit.git lib/jonkit`

6. Add the JonKit lib into your Xcode project by going to File > Add files to "project", then select lib/jonkit/JonKit.xcodeproj

7. Link the framework to your project and ensure it is compiled each time you compile your project, select your project (usually the top item) in the left hand project browser tree and then select your project's app target from the top left of the main pane (should be a general app icon next to it, rather than a blue Xcode file icon), then select the "General" tab next to where you just selected the target. Now, goto "Embedded Binaries" at the bottom of the main pane, add and select the JonKit framework target (If there's two, just pick the top one, they both point to the same place anyway). This should now add the framework to both "Embedded Binaries" and "Linked Frameworks and Libraries", don't worry about the red text, that's just because the framework binary doesn't exist until you build your project.

8. Do a quick build of the app (the play button in the top left of Xcode), you won't be able to see anything yet but it's worth checking to make sure everything is working ok so far, if you get an error, try re-tracing the above steps and see if you missed anything. If you see a plain white screen in the simulator, you've got a successful build, stop the simulator and move to the next step.

9. Go into your main target settings (project file in top left of project tree, then select your app under "targets" in the drop down menu in the top left of the main pane) and clear everything in "Main Interface" under "Deployment Info". You can now delete the "Main.Storyboard" file and the "ViewController.swift" file as they are no longer used by your project (select "Move to Trash" when prompted).

10. Now you're going to update your AppDelegate class to use the Framework code, in your project browser expand the "JonKit.xcodeproj" tree and under "Templates" you will find another AppDelegate.swift, copy all of the code in this file and paste into the AppDelegate.swift file in your project, replacing all of the code in there with the code from the template.

11. Add the JonKitAssets bundle to your target's dependencies, Go into the "Build Phases" tab of your main target settings, click the plus underneath "Target Dependencies" and select "JonKitAssets".

12. Add the JonKitAssets bundle to "Copy Bundle Resources", to do this, go to the same screen as used in the above step then expand the "Products" folder under "JonKit" from the project browser on the left, now drag and drop the JonKitAssets.bundle over the "Copy Bundle Resources" section, it should stick.

13. Copy the template app config file into your project, first create a new group called "Config" within your project's main group. To create a new group, just right click over the group you want to create it within, select "New Group" and name it. Now you can copy the template config file in from the library, to do this, right click over the "Config" group you just created, select "Add files to [project]", now browse to lib/jonkit/Templates and select "app.json". **IMPORTANT**: Ensure that "Copy items if needed" is selected, otherwise your app will be using the example config file from the lib, rather than its own copy and if the lib template is updated, your app config will change too.

14. Try and build, you *should* have a working app wrapper! If not, try and re-trace your steps, make sure everything is set up as specified above.

## Optional Steps

- Add a settings bundle to your app (these are the options that appear in the main Settings app for each third party app), to do this, go to the "Build Phases" settings of your main target, click the plus in the top left of the main pane, select "New Run Script Phase", then clear the text in the textbox underneath "Shell" and type `lib/jonkit/Settings.sh`. This will run the settings script that copies over the correct Settings.bundle (depending on if it is a Release or a Debug build)  into your app on each build. To test that it's worked, do a build and when the app starts in the simulator, return to the home screen (Cmd + Shift + H) the select the "Settings" app, scroll to the bottom and you should be able to see an entry for your app.

***

# JS Bridge Integration Guide

## What is it?

The JS Bridge is a utility built into the JonKit library that allows data to be sent from the web layer to the native app and visa versa, via javascript event messaging.

## How does it work?

Each time the app finishes loading a new web page into the main frame, it injects a Javascript object into the global JS namespace that allows the website to send & receive messages, coupled with a data object, to and from the app wrapper.

Once the website has loaded within the app you should have access to an instance of the JonKitApp class in the global namespace, available at:

    var app = window.document.jonKit.app;

## Sending messages

To send a message to the app wrapper:

    var messageData = {"someMessageData": "test", "moreData": true};
    app.sendMessage('aMessageName', messageData);

You shouldn't ever need to do this in practice as all the app side handled events should be wrapped in one of the app functions below.

## Receiving messages

When the native wrapper sends messages down to the website the events are emitted by the App object, to listen for an event/message you're interested in:

    app.bind('someInterestingMessage', function(data) {
        // ... Do something with the message data here
    });

OR

    app.bind('someInterestingMessage', somePredefinedFunction);

When the native wrapper fires a 'someInterestingMessage' the callback you've bound to that message will be called. The data parameter is not always guaranteed to be present, it depends on which message is currently being received, please bear in mind that data may be 'undefined'.

The structure of the data object changes depending on the message, please see the specific message descriptions below to see which messages contain what data.

## App -> Web Messages

## App functions (Web -> App)
These functions act as wrappers around available app functionality and send messages to the app on your behalf.

If the function was introduced after v1.0 you should check for the existence of these functions before calling, as the version of the app wrapper you're running in may not have that function available.

### getAppInfo
*Since v1.0*

    var appInfo = app.getAppInfo();
    var appName = appInfo.name;

Returns a blob of data about the app/system/device

**Returns** *Object*

    {
        name: "app name",
        version: "1.0",
        build: "123",
        device: "iPhone",
        OSname: "iPhone OS",
        OSversion: "8.3"
    }  

### hideStatusBar
*Since v1.0*

    app.hideStatusBar();

Completely hides the app's status bar, including the time and network provider.

### showStatusBar
*Since v1.0*

   app.showStatusBar();

Shows the status bar

### setStatusBarStyle
*Since v1.0*

    app.setStatusBarStyle(hidden, textStyle, bgColour);

Allows you to customise the appearance of the app's status bar:

* **hidden** *Bool* - Whether to show or hide the status bar text

* **textStyle** *String* - Either 'light' or 'dark', you should choose based on the background the status bar is displayed on, 'dark' is the default value

* **bgColour** *String* - A string representation of the RGBA values of the colour you would like the background of the status bar to appear as e.g.: 'rgba(0,0,0,1)' would draw a solid black status bar, 'rgba(255,0,0,0.5)' would draw a red status bar with 50% transparency. If you don't want any background use 'none'. If the background is not drawn or is not fully solid, the web content will appear behind the status bar, if you choose to display the status bar in this way you will need to add 20px padding to the top of each web page.

Examples:

    // hides the status bar, same as app.hideStatusBar()
    app.setStatusBarStyle(true);

    // light text on solid black bg
    app.setStatusBarStyle(false, 'light', 'rgba(0,0,0,1)');

    // dark text on light grey bg with 80% opacity
    app.setStatusBarStyle(false, 'dark', 'rgba(220,220,220,0.8)');

    // dark text, no bg
    app.setStatusBarStyle(false, 'dark', 'none');

### enablePullToRefresh
*Since v1.0*

    app.enablePullToRefresh();

Enable the pull down to refresh feature on the current webview

### disablePullToRefresh
*Since v1.0*

    app.disablePullToRefresh();

Disable the pull down to refresh feature on the current webview

***

# iOS Asset List

## Required

**App Icons**

- 29 x 29
- 40 x 40
- 58 x 58
- 76 x 76
- 87 x 87
- 80 x 80
- 120 x 120
- 152 x 152
- 180 x 180

All icons should be fully square, rounded corners will be added by the system. Unlike Android, alpha channels are not supported for the icons, so the background must be fully opaque.

## Optional

**Launch Screen Background**

- 2208 x 2208

This will be resized at build/run time for the current device

**Other Launch Screen Images**

App/Product Logo etc, all assets supplied must also be provided in *@2x* and *@3x* to support all iOS device screens (If the app does not need to support iPads, we only need *@2x* and *@3x*).

**App store Icons**

- 512 x 512
- 1024 x 1024

Same as the app icon, fully square, rounded corners will be added later.
