function FodApp() {
    this.scheme = 'fodappjs'
    this.iframe = document.createElement("IFRAME");
    this.appInfo = {};
    
    // Init
    
    // tell the app we're not ready to receive messages each time we go away
    var currentUnloadHandler = window.onunload
    window.onunload = function() {
        if(currentUnloadHandler) currentUnloadHandler();
        this.ready(false);
    }.bind(this);
    
    // App messaging
    
    this.sendMessage = function(message, data) {
        var dataString = JSON.stringify(data);
        var url = this.scheme + '://' + message + '#' + ((dataString) ? dataString : '');
        this.iframe.setAttribute("src", url);
        document.documentElement.appendChild(this.iframe);
        this.iframe.parentNode.removeChild(this.iframe);
    }
    
    this.receiveMessage = function(message, dataString) {
        var data = {};
        if (dataString) {
            data = JSON.parse(dataString);
        }
        this.trigger(message, data);
    }
    
    // Event handling (MicroEvent.js https://github.com/jeromeetienne/microevent.js)
    
    this.bind = function(event, fct) {
        this._events = this._events || {};
        this._events[event] = this._events[event] || [];
        this._events[event].push(fct);
    }

    this.unbind = function(event, fct) {
        this._events = this._events || {};
        if( event in this._events === false  )	return;
        this._events[event].splice(this._events[event].indexOf(fct), 1);
    }

    this.trigger = function(event /* , args... */) {
        this._events = this._events || {};
        if( event in this._events === false  )	return;
        for(var i = 0; i < this._events[event].length; i++){
            this._events[event][i].apply(null, Array.prototype.slice.call(arguments, 1));
        }
    }
    
    // App functions
    
    // tell the app if we're ready or not to start receiving messages
    this.ready = function(ready) {
        ready = (ready != false) || false;
        this.sendMessage('jsBridgeWebReady', {'ready': ready});
    }
    
    // get info about the app/system/device
    this.getAppInfo = function() {
        return this.appInfo;
    }
    
    // completely hides the app's status bar including the time
    this.hideStatusBar = function() {
        this.sendMessage('hideStatusBar');
    }

    this.showStatusBar = function() {
        this.sendMessage('showStatusBar');
    }
    
    // control the appearence of the app's status bar
    this.setStatusBarStyle = function(hidden, textStyle, bgColour) {
        var data = {
            "hidden": hidden,
            "textStyle": textStyle,
            "bgColour": bgColour
        };
        this.sendMessage('setStatusBarStyle', data);
    };

    this.enablePullToRefresh = function() {
        this.sendMessage('enablePullToRefresh')
    }

    this.disablePullToRefresh = function() {
        this.sendMessage('disablePullToRefresh')
    }
    
    // App message handlers
    this.bind('appInfo', function(data) {
        if(data) {
            this.appInfo = data;
        }
    }.bind(this));

}

window.document.fodapp = new FodApp()
window.document.fodapp.ready(true)