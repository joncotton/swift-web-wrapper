//
//  Config.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 15/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

public class ConfigParser {
    
    public var rawConfig:[String:AnyObject] = Dictionary()
    
    public func parse(rawConfig:[String:AnyObject]) {
        self.rawConfig = rawConfig
    }
    
    public func unpack<T>(key: String, inout Into destination: T?) {
        if let
            configParser = destination as? ConfigParser,
            value = self.rawConfig[key] as? [String:AnyObject]
        {
            configParser.parse(value)
        } else if let value = self.rawConfig[key] as? T {
            destination = value
        }
    }
    
    // also provide an non-optional interface to the unpack method
    public func unpack<T>(key: String, inout Into destination: T) {
        var optionalDestination = Optional(destination)
        self.unpack(key, Into: &optionalDestination)
    }

}