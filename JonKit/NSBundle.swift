//
//  NSBundle.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 30/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

/**
    Adds a new convenience class method to access the FodAssets Bundle.
*/
extension NSBundle {
    
    public class func FodAssetsBundle() -> NSBundle? {
        if let bundlePath = NSBundle.mainBundle().pathForResource("FodAssets", ofType: "bundle") {
            return NSBundle(path: bundlePath)
        }
        
        return nil
    }
    
}