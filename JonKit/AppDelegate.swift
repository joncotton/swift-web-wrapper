//
//  AppDelegate.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 20/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

/**
    Includes core fucntionality to set up the app, your app's appDelegate should
    extend this class rather than UIApplicationDelegate
*/
public class AppDelegate: UIResponder, UIApplicationDelegate {
    
    /// The filename of the main app config json file to load, this can be overriden
    public var appConfigFilename = "app"
    
    /// The app's main window
    public var window: UIWindow?
    
    private var _rootViewController: RootViewController?
    
    private var lastActiveTime: NSDate?
    
    private var secondsBeforeConfigRefresh: NSTimeInterval = (60 * 60) //1hour
    
    /**
        called after a cold launch of the app
    
        :param: application The application object.
        :param: launchOptions A dictionary containing any options passed by the system at app launch, see http://nshipster.com/launch-options/
    
        :returns: Bool Whether or not the app initialisation was sucessful, returning false means the app will fail to launch
    */
    public func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        NSURLProtocol.registerClass(UserAgentUrlProtocol)
        
        // load the app config
        let configLoader = ConfigLoader()
        configLoader.loadAppConfig(self.appConfigFilename)
        
        let config = Config.SharedInstance
        config.parse(configLoader.rawConfig())
        
        // set up the RVC
        let rootViewController = self.rootViewController()
        rootViewController.load(config)
        
        // set up the main window
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        self.window?.makeKeyAndVisible()
        self.window?.rootViewController = rootViewController
        
        return true
    }
    
    public func applicationWillResignActive(application: UIApplication) {
        // grab a timestamp of when the app is going away
        self.lastActiveTime = NSDate()
    }
    
    public func applicationWillEnterForeground(application: UIApplication) {
        if let rootViewController = self.window?.rootViewController as? LoadableViewController {
            var intervalSinceLastActive:NSTimeInterval = 0.0
            if let lastActiveTime = self.lastActiveTime {
                intervalSinceLastActive = NSDate().timeIntervalSinceDate(lastActiveTime)
            }
            
            let config = Config.SharedInstance
            var refreshedConfig:Config?
            var secondsBeforeConfigRefresh = self.secondsBeforeConfigRefresh
            if let newSecondsBeforeConfigRefresh = config.inactiveSecondsBeforeConfigRefresh {
                secondsBeforeConfigRefresh = newSecondsBeforeConfigRefresh
            }
            
            if intervalSinceLastActive > secondsBeforeConfigRefresh {
                // reload the config
                let configLoader = ConfigLoader()
                configLoader.loadAppConfig(self.appConfigFilename)

                config.parse(configLoader.rawConfig())
                refreshedConfig = config
            }

            rootViewController.loadFromBackground(intervalSinceLastActive, config: refreshedConfig)
        }
    }
    
    /**
        Returns a new instance of RootViewController, if you need to use a custom RVC, override
        this method and return an instance of your custom subclass of RootViewController.
    
        :param: Config A config object representing all of the main app config to be passed into the RootViewController
    
        :returns: RootViewController A new instance of RootViewController or one its descendants
    */
    public func rootViewController() -> RootViewController {
        if self._rootViewController == nil {
            self._rootViewController = RootViewController()
        }
        return self._rootViewController!
    }
    
}


