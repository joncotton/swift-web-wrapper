//
//  JSBridge.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 22/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

public class JSBridge {
    
    static let Scheme = "fodappjs"
    private var appInfo:[String:String]?
    
    var messageQueue: [String:String] = Dictionary()
    var receiverIsReady = false {
        didSet {
            if self.receiverIsReady == true {
                for (message, dataString) in self.messageQueue {
                    self.sendJsMessageToWebView(message, jsonString: dataString)
                }
                messageQueue.removeAll(keepCapacity: false)
            }
        }
    }
    
    private var registeredHandlers: [String: (data:[String:AnyObject]?) -> ()] = Dictionary()
    var jsonParser = JSONParser()
    var webView: UIWebView?
    
    public init() {
        self.registerMessageHandler("jsBridgeWebReady") { (data) -> () in
            if let data  = data,
                   ready = data["ready"] as? Bool
            {
                self.receiverIsReady = ready
            }
        }
        
        self.sendAppInfo()
    }
    
    func handleUrl(url: NSURL) {
        if url.scheme != JSBridge.Scheme {
            return
        }
        
        var dataString: String? = nil
        if let urlFragment = url.fragment {
            if urlFragment != "" {
                dataString = NSString(string: urlFragment).stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
            }
        }
        
        let message = url.host!
        if let registeredHandler = self.registeredHandlers[message] {
            var dataDictionary:[String:AnyObject]?
            if let dataString = dataString {
                dataDictionary = self.jsonParser.contentsOfString(dataString)
            }
            
            registeredHandler(data: dataDictionary)
        }
    }
    
    public func registerMessageHandler(message: String, handler: (data: [String: AnyObject]?) -> ()) {
        self.registeredHandlers[message] = handler
    }
    
    public func sendMessage(message: String, data: [String: AnyObject]?) {
        var jsonString: String?
        if let data = data {
            jsonString = self.jsonParser.jsonStringFromDictionary(data)
        }
        
        if(self.receiverIsReady) {
            self.sendJsMessageToWebView(message, jsonString:jsonString)
        } else {
            self.messageQueue[message] = jsonString
        }
    }
    
    private func sendJsMessageToWebView(message: String, jsonString: String?) {
        if let webView = self.webView {
            var jsString = ""
            if let jsonString = jsonString {
                let unescapedJsonString = jsonString.stringByReplacingOccurrencesOfString("\\", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                jsString = "window.document.fodapp.receiveMessage('\(message)', '\(unescapedJsonString)');"
            } else {
                jsString = "window.document.fodapp.receiveMessage('\(message)');"
            }

            webView.stringByEvaluatingJavaScriptFromString(jsString)
        }
    }
    
    func javascriptCode() -> String {
        if let fodBundle = NSBundle.FodAssetsBundle() {
            let jsFile = "jsBridge.js"
            let jsFilePath = fodBundle.pathForResource(jsFile, ofType: nil)!
            return NSString(contentsOfFile: jsFilePath, encoding: NSUTF8StringEncoding, error: nil) as! String
        } else {
            return ""
        }
    }
    
    private func sendAppInfo() {
        if self.appInfo == nil {
            if let appInfo = NSBundle.mainBundle().infoDictionary {
                let device = UIDevice.currentDevice()
                let appName = appInfo["CFBundleName"] != nil ? appInfo["CFBundleName"] as? String : ""
                let appVersion = appInfo["CFBundleShortVersionString"] != nil ? appInfo["CFBundleShortVersionString"] as? String : ""
                let appBuildNumber = appInfo["CFBundleVersion"] != nil ? appInfo["CFBundleVersion"] as? String : ""

                self.appInfo = [
                    "name": appName!,
                    "version": appVersion!,
                    "build": appBuildNumber!,
                    "device": device.model,
                    "OSname": device.systemName,
                    "OSversion": device.systemVersion
                ]
            }
        }
        
        self.sendMessage("appInfo", data: self.appInfo)
    }

}