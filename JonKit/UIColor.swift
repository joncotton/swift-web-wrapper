//
//  UIColor.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 30/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

/**
    Adds the ability to instantiate a new UIColor object using a String in the rgba(r,b,g,a) format.
    Also adds convenience computed properties to return the individual RGBA values from a UIColor object.
*/
extension UIColor {
    

    /// Value of the red channel.
    var red: CGFloat {
        let components = CGColorGetComponents(self.CGColor);
        return components[0]
    }
    
    /// Value of the green channel.
    var green: CGFloat {
        let components = CGColorGetComponents(self.CGColor);
        return components[1]
    }
    
    /// Value of the blue channel.
    var blue: CGFloat {
        let components = CGColorGetComponents(self.CGColor);
        return components[2]
    }
    
    /// Value of the alpha channel (transparency).
    var alpha: CGFloat {
        let components = CGColorGetComponents(self.CGColor);
        return components[3]
    }
    
    /**
        Intialise a new UIColor object using a String in the rgba(r,b,g,a) format. If the rgbaString param
        can't be parsed a solid black colour will be initialised

        :param: rgbaString A string into format of rgba(0-255,0-255,0-255,0-1)
    */
    convenience init(rgbaString: String) {
        if rgbaString.hasPrefix("rgba(") {
            let colourValuesRange = Range(start: advance(rgbaString.startIndex, 5), end: advance(rgbaString.endIndex, -1))
            let colourValuesString = rgbaString.substringWithRange(colourValuesRange)
            let colourValues = colourValuesString.componentsSeparatedByString(",")
            
            let red = CGFloat((colourValues[0] as NSString).doubleValue) / 255.00
            let green = CGFloat((colourValues[1] as NSString).doubleValue) / 255.00
            let blue = CGFloat((colourValues[2] as NSString).doubleValue) / 255.00
            let alpha = CGFloat((colourValues[3] as NSString).doubleValue)
            
            self.init(red: red, green: green, blue: blue, alpha: alpha)
        } else {
            // default to black
            FodLog("Failed to init UIColor from rgba string \(rgbaString)", .Warning)
            self.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }
    }
    
}