//
//  LoadingViewController.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 05/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

public class LoadingViewController : BaseViewController {
    
    private var shouldUseAppLaunchScreenAsBackground = true
    
    private var shouldDisplayAppVersionNumber = true
    
    private var tintColour = UIColor.blackColor()
    
    private var bgColour = UIColor.whiteColor()
    
    
    @IBOutlet public var loadingIndicatorView: UIView?
    
    @IBOutlet public var messageLabel: UILabel?
    
    @IBOutlet public var retryButton: UIButton?
    
    private var retryAction: (() -> ())?
    
    private var appLaunchScreenView: UIView? {
        get {
            let mainBundle = NSBundle.mainBundle()
            if let
                infoDictionary = mainBundle.infoDictionary,
                nibName = infoDictionary["UILaunchStoryboardName"] as? String,
                nibViews:NSArray = mainBundle.loadNibNamed(nibName, owner: self, options:nil),
                launchScreenView = nibViews.objectAtIndex(0) as? UIView
            {
                return launchScreenView
            }
            
            return nil
        }
    }
    
    // MARK: - Init
    
    public required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    public convenience init() {
        NSBundle.FodAssetsBundle()?.load()
        self.init(nibName: "LoadingView", bundle: NSBundle.FodAssetsBundle())
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.showLoadingIndicator()
    }
    
    public func showLoadingIndicator() {
        self.hideMessage()
        self.hideRetryButton()

        self.loadingIndicatorView?.hidden = false
    }
    
    public func hideLoadingIndicator() {
        self.loadingIndicatorView?.hidden = true
    }
    
    public func showMessage(message: String) {
        self.hideLoadingIndicator()
        
        self.messageLabel?.text = message
        self.messageLabel?.hidden = false
    }
    
    public func hideMessage() {
        self.messageLabel?.hidden = true
    }
    
    public func showError(message: String, retryAction: (() -> ())?) {
        self.showMessage(message)
        
        if retryAction != nil {
            self.retryAction = retryAction
            self.showRetryButton()
        }
    }
    
    public func showRetryButton() {
        self.retryButton?.hidden = false
    }
    
    public func hideRetryButton() {
        self.retryButton?.hidden = true
    }
    
    @IBAction func retryButtonWasTouched(sender: UIButton) {
        if let retryAction = self.retryAction {
            self.showLoadingIndicator()
            retryAction()
        }
    }
    
    override public func load(config: Config) {
        let loadingViewConfig = config.loadingViewConfig
        
        self.shouldUseAppLaunchScreenAsBackground ?= loadingViewConfig.useLaunchScreenAsBackground
        self.tintColour ?= loadingViewConfig.tintColour
        self.bgColour ?= loadingViewConfig.bgColour

        let statusBarConfig = loadingViewConfig.statusBarConfig
        self.statusBarIsHidden ?= statusBarConfig.hidden
        self.statusBarTextStyle ?= statusBarConfig.textStyle
        self.statusBarBackgroundColour ?= statusBarConfig.bgColour
        
        if self.shouldUseAppLaunchScreenAsBackground {
            if let launchScreenView = self.appLaunchScreenView {
                self.view.addSubviewAtFullWidthFullHeight(launchScreenView)
                self.view.sendSubviewToBack(launchScreenView)
            }
        }
        
        self.retryButton?.tintColor = self.tintColour
        self.retryButton?.setTitle("Retry".localized, forState: .Normal)
        
        self.view.backgroundColor = self.bgColour
    }
    
}