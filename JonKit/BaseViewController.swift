//
//  BaseViewController.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 29/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

public protocol LoadableViewController {
    var readyAction: ((vc: UIViewController) -> ())? { get set }
    var errorAction: ((error: NSError) -> ())? { get set }
    
    func load(config: Config)
    func loadFromBackground(interval: NSTimeInterval, config: Config?)
}

public class BaseViewController : UIViewController, LoadableViewController {
    
    // MARK: - Status Bar
    
    var statusBarIsHidden = false {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var statusBarTextStyle = UIStatusBarStyle.Default {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var statusBarBackgroundColour: UIColor? {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override public func prefersStatusBarHidden() -> Bool {
        return self.statusBarIsHidden
    }
    
    override public func preferredStatusBarStyle() -> UIStatusBarStyle {
        return self.statusBarTextStyle
    }
    
    public func preferredStatusBarBackgroundColour() -> UIColor? {
        return self.statusBarBackgroundColour
    }
    
    // MARK: - LoadableViewController protocol methods
    
    public var readyAction: ((vc: UIViewController) -> ())?
    
    public var errorAction: ((error: NSError) -> ())?
    
    /// Basic implementation of this method, you're almost definitely gonna wanna override this and not call super
    public func load(config: Config) {
        if let readyAction = self.readyAction {
            readyAction(vc: self)
        }
    }
    
    public func loadFromBackground(interval: NSTimeInterval, config: Config?) {
        if let readyAction = self.readyAction {
            readyAction(vc: self)
        }
    }

}