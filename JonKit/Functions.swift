//
//  Functions.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 01/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

/**
    Log Message Severity Levels

    - Debug     - The default log level, used during development
    - Info      - Useful info about execution path/state
    - Notice    - Potential issue but not yet a full blown error
    - Warning   - Unexpected condition, will likely cause an error if not addressed
    - Error     - Something has failed, needs to be looked into
    - Fatal     - Unrecoverable error, app will probably crash
*/
public enum LogLevel: String {
    case Debug = "DEBUG"
    case Info = "INFO"
    case Notice = "NOTICE"
    case Warning = "WARN"
    case Error = "ERROR"
    case Fatal = "FATAL"
}

/**
    General prurpose function for app logging

    There's no need to pass the file, function or line params, these will
    be automatically determined when you call the function.

    :param: message The log message
    :param: level   The log level
*/
public func FodLog(
    message: String,
    _ level: LogLevel = .Debug,
    file: NSString = __FILE__,
    function: String = __FUNCTION__,
    line: Int = __LINE__
) {
    var originInfo = ""
    
    #if DEBUG
    let shortFile = NSString(string:file).lastPathComponent.stringByDeletingPathExtension
    originInfo = "[\(shortFile).\(function):\(line)]"
    #endif
    
    switch level {
    case .Debug, .Info:
        #if DEBUG
        println("[\(NSDate().isoFormat)] [\(level.rawValue)] \(originInfo) \(message)")
        #endif
            
    default:
        println("[\(NSDate().isoFormat)] [\(level.rawValue)] \(originInfo) \(message)")
    }
}