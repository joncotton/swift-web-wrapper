//
//  FodUrlValidator.swift
//  Fleet On Demand Wrapper
//
//  Created by Jonathan Cotton on 13/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

public class UrlValidator {
   
    /**
        Possible results from the URL validator when checking a URL

        - Internal: The URL is known and should be handled by the app.
        - NotHandled: The URL is not known.
     */
    public enum Result {
        case Internal
        case NotHandled
    }
    
    let urlConfig: UrlConfig
    
    /**
        Designated intialiser
    
        :param: config The UrlConfig object to initalise with, the properties of this object will be used when checking against URLs.
     */
    public init(config: UrlConfig) {
        urlConfig = config
    }
    
    /**
        Check the URL against known URL patterns and return a UrlValidator.Result.

        :param: url The URL to check.
    
        :returns: UrlValidator.Result
    */
    public func checkUrl(url: NSURL) -> UrlValidator.Result {
        if urlIsInList(url, patterns: urlConfig.internalUrls) {
            return UrlValidator.Result.Internal
        }
        
        return UrlValidator.Result.NotHandled
    }
    
    private func urlIsInList(url: NSURL, patterns: [String:String]) -> Bool {
        let urlString = url.absoluteString!
        for (key, pattern) in patterns {
            let fullUrlPattern = "^((http|https):\\/\\/" + pattern + "(\\/)*)$"
            if let regex = NSRegularExpression(pattern: fullUrlPattern, options: nil, error: nil) {
                let range = NSRange(location: 0, length: count(urlString))
                if regex.numberOfMatchesInString(urlString, options: nil, range: range) > 0 {
                    return true
                }
            }
        }

        return false
    }
    
}
