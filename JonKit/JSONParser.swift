//
//  JSONParser.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 17/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

public class JSONParser {
    
    public init() {}
    
    public func contentsOfLocalFile(filename: String) -> [String: AnyObject]? {
        if let
            path = NSBundle.mainBundle().pathForResource(filename, ofType: "json"),
            url  = NSURL(fileURLWithPath: path)
        {
            return contentsOfUrl(url)
        } else {
            FodLog("Failed to load local JSON file: \(filename).json", .Warning)
            return nil
        }
    }
    
    public func contentsOfUrl(url: NSURL)  -> [String: AnyObject]? {
        // TODO: This is currently a blocking request, so if it times out the app will just sit there waiting, ideally this should be an async request so we can carry on with the load process whilst retrieving any overridden config
        if let
            data     = NSData(contentsOfURL: url),
            jsonData = self.contentsOfData(data)
        {
            return jsonData
        } else {
            FodLog("Failed to parse JSON file at URL: \(url.lastPathComponent!)", .Warning)
            return nil
        }
    }
    
    public func contentsOfString(string: String) -> [String: AnyObject]? {
        if let
            data  = NSString(string: string).dataUsingEncoding(NSUTF8StringEncoding),
            jsonData = self.contentsOfData(data)
        {
            return jsonData
        } else {
            FodLog("Failed to parse JSON file from String: \(string)", .Warning)
            return nil
        }
    }
    
    private func contentsOfData(data: NSData) -> [String: AnyObject]? {
        var jsonError: NSError?
        
        if let jsonData = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &jsonError) as? [String: AnyObject] {
            return jsonData
        } else {
            if let error = jsonError {
                FodLog("JSON Parsing Error: \(error.localizedDescription)", .Error)
            }
            
            return nil
        }
    }
    
    public func jsonStringFromDictionary(dict: [String: AnyObject]) -> String? {
        var jsonError: NSError?
        
        if let
            jsonData = NSJSONSerialization.dataWithJSONObject(dict, options: nil, error: &jsonError),
            jsonString = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as? String
        {
            return jsonString
        } else {
            FodLog("Failed to endocde dictionary as JSON: \(dict.description)", .Warning)
            return nil
        }
    }
    
}