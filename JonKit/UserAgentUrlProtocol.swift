//
//  UserAgentUrlProtocol.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 20/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation


public class UserAgentUrlProtocol : NSURLProtocol, NSURLConnectionDelegate {
    
    static let handledKey = "UserAgentUrlProtocolHandled"
    private static var newUserAgent: String? = nil
    
    let userAgentHeaderKey = "User-Agent"
    var connection: NSURLConnection!

    override public class func canInitWithRequest(request: NSURLRequest) -> Bool {
        if NSURLProtocol.propertyForKey(UserAgentUrlProtocol.handledKey, inRequest: request) != nil {
            return false
        }
        
        return true
    }
    
    override public class func canonicalRequestForRequest(request: NSURLRequest) -> NSURLRequest {
        return request
    }
        
    override public func startLoading() {
        var newRequest = self.request.mutableCopy() as! NSMutableURLRequest
        NSURLProtocol.setProperty(true, forKey: UserAgentUrlProtocol.handledKey, inRequest: newRequest)
        
        if UserAgentUrlProtocol.newUserAgent == nil {
            let config = self.globalConfig()
            if let userAgentSuffix = config.userAgentSuffix,
                currentUserAgent = self.request.valueForHTTPHeaderField(userAgentHeaderKey) {
                UserAgentUrlProtocol.newUserAgent = currentUserAgent + " " + userAgentSuffix
            }
        }

        if let newUserAgent = UserAgentUrlProtocol.newUserAgent {
            newRequest.setValue(newUserAgent, forHTTPHeaderField: userAgentHeaderKey)
        }

        self.connection = NSURLConnection(request: newRequest, delegate: self)
    }
    
    override public func stopLoading() {
        if self.connection != nil {
            self.connection.cancel()
        }
        self.connection = nil
    }
    
    func connection(connection: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        self.client!.URLProtocol(self, didReceiveResponse: response, cacheStoragePolicy: .NotAllowed)
    }
    
    func connection(connection: NSURLConnection!, didReceiveData data: NSData!) {
        self.client!.URLProtocol(self, didLoadData: data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection!) {
        self.client!.URLProtocolDidFinishLoading(self)
    }
    
    public func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        self.client!.URLProtocol(self, didFailWithError: error)
    }
    
    private func globalConfig() -> Config {
        return Config.SharedInstance;
    }

}