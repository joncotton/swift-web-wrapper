//
//  String.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 12/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

extension String {

    private var config:Config {
        return Config.SharedInstance
    }
    
    private var language:String {
        if let language = NSBundle.mainBundle().preferredLocalizations.first as? String {
            return language
        }
        
        return "en"
    }
    
    public var localized:String {
        if let localizedString = self.config.strings[self.language]?[self] {
            return localizedString
        }
        
        return self
    }
    
}
