//
//  NSDate.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 01/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

/**
    Adds a new isoFormat computed property to NSDate to get the current timestamp as an ISO formatted string
*/
extension NSDate {
    var isoFormat: String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        return formatter.stringFromDate(self)
    }
}