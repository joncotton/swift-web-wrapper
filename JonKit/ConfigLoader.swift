//
//  ConfigLoader.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 07/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

/**
    Responsible for loading config from local JSON files, remote JSON files via http(s) and any
    debug overrides specified in a debug build
*/
public class ConfigLoader {

    /// The JSONParer object to use when decoding JSON data
    public let jsonParser: JSONParser
    
    private var configData: [String: AnyObject] = Dictionary()
    
    /**
        Designated initaliser
    
        :param: jsonParser A JSONParser object to use to decode JSON data
    */
    public init(jsonParser: JSONParser) {
        self.jsonParser = jsonParser
    }
    
    public convenience init() {
        self.init(jsonParser: JSONParser())
    }
    
    // MARK:- load config
    
    /**
        Kicks of the main app config loading process by first loading the json file specified
        by the filename param, if we're running in debug mode, it checks for any debug overrides,
        then finally loads the remote json file as specified by the remoteConfigUrl directive
        from either the local file or the debug overrides, if there isn't a remoteConfigUrl key
        present in the config, nothing is loaded. The loaded config is stored within this object
        and can be accessed using either valueForKey or subscripting.
    
        :param: filename The name of the JSON file, without the .json extension, to load
    */
    public func loadAppConfig(filename: String) {
        FodLog("Loading app config from: \(filename).json")
        self.loadFile(filename)
        
        #if DEBUG
            self.loadDebugOverrides()
        #endif
        
        if let remoteConfigUrlString = self.configData[Config.Key.RemoteConfigUrl] as? String,
            remoteConfigUrl = NSURL(string:remoteConfigUrlString) {
                FodLog("Attempting to load remote config from: \(remoteConfigUrl)", .Info)
                self.loadUrl(remoteConfigUrl)
        }
    }
    
    /**
        Load a JSON file that exists within the app bundle and add it's data to
        the existing config data held within this object.
        
        :param: filename The name of the JSON file, without the .json extension, to load
    */
    public func loadFile(filename: String) {
        if let fileContents = jsonParser.contentsOfLocalFile(filename) {
            self.configData += fileContents
        } else {
            FodLog("Failed to load local JSON config file: \(filename)", .Warning)
        }
    }
    
    /**
        Load a remote JSON file at the specified URL and add it's data to the existing
        config data held within this object.
        
        :param: url An NSURL object that points to the location of a JSON file
    */
    public func loadUrl(url: NSURL) {
        if let urlContents = jsonParser.contentsOfUrl(url) {
            self.configData += urlContents
        } else {
            FodLog("Failed to load JSON config file from URL: \(url)", .Warning)
        }
    }
    
    private func loadDebugOverrides() {
        FodLog("Checking for debug overrides")
        let userDefaults = NSUserDefaults.standardUserDefaults()
        
        // Environment overrides
        if let environment = userDefaults.stringForKey("Environment") {
            if (environment != "live") {
                FodLog("Loading config overrides for \(environment) environment")
                self.loadFile(environment)
            }
        }
        
        // Remote config override
        if let remoteConfigUrl = userDefaults.stringForKey("remoteConfigUrl") {
            self.configData[Config.Key.RemoteConfigUrl] = remoteConfigUrl
        }
    }
    
    public func rawConfig() -> [String: AnyObject] {
        return self.configData
    }

}