//
//  LoadingViewConfig.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 11/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

public class LoadingViewConfig : ConfigParser {
    
    public struct Key {
        public static let UseLaunchScreenAsBackground = "UseLaunchScreenAsBackground"
        public static let TintColour                  = "TintColour"
        public static let BgColour                    = "BgColour"
        public static let StatusBarConfig             = "StatusBarConfig"
    }
    
    public var useLaunchScreenAsBackground: Bool?
    public var tintColour: UIColor?
    public var bgColour: UIColor?
    public var statusBarConfig = StatusBarConfig()
    
    override public func parse(rawConfig: [String: AnyObject]) {
        super.parse(rawConfig)
        
        var rawTintColour:String?
        var rawBgColour:String?
        
        self.unpack(Key.UseLaunchScreenAsBackground, Into: &useLaunchScreenAsBackground)
        self.unpack(Key.TintColour, Into: &rawTintColour)
        self.unpack(Key.BgColour, Into: &rawBgColour)
        self.unpack(Key.StatusBarConfig, Into: &self.statusBarConfig)
        
        if let rawTintColour = rawTintColour {
            self.tintColour = UIColor(rgbaString: rawTintColour)
        }
        
        if let rawBgColour = rawBgColour {
            self.bgColour = UIColor(rgbaString: rawBgColour)
        }
    }
}