//
//  JonKit.h
//  JonKit
//
//  Created by Jonathan Cotton on 14/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for JonKit.
FOUNDATION_EXPORT double JonKitVersionNumber;

//! Project version string for JonKit.
FOUNDATION_EXPORT const unsigned char JonKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JonKit/PublicHeader.h>


