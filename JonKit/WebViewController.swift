//
//  FodWebViewController.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 14/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit
import WebKit

public typealias WebRequestCompletionHandler = (url: NSURL) -> ()
public typealias WebRequestFailureHandler = (error: NSError) -> ()

/**
    A controller that handles interactions with a webview, the behaviour of the webview can be configured via
    the WebViewConfig object passed into the controller at Init.
 */
public class WebViewController: BaseViewController, UIWebViewDelegate, UIScrollViewDelegate {
    
    var timeout:NSTimeInterval = 30.0

    var webView: UIWebView! // unwrapped because it is always set in viewDidLoad
    
    var initialUrl = NSURL(string:"https://www.fleetondemand.com/")!
    private var urlValidator: UrlValidator?
    public var jsBridge: JSBridge?
    var shouldShowLoadingOverlay = false
    var pullToRefreshIsEnabled = false {
        didSet{
            self.refreshIndicator.hidden = !self.pullToRefreshIsEnabled
        }
    }

    private var isRefreshing = false
    private var refreshIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
    
    private var requestTimer = NSTimer()
    
    private var isInitialLoad = false
    
    public var configKey = "Main"
    
    private var inactiveSecondsBeforeRefresh:NSTimeInterval = (60.0 * 30.0) // 30mins

    // MARK: - Init / Loadable View Controller Protocol
    
    override public func load(config: Config) {
        if let webViewConfig = config.webViewConfig[self.configKey] {
            self.isInitialLoad = true
            
            self.initialUrl ?= webViewConfig.initialUrl
            
            self.urlValidator = UrlValidator(config:webViewConfig.urlConfig)
            
            if let enableJSBridge = webViewConfig.enableJsBridge {
                if enableJSBridge {
                    self.jsBridge = JSBridge()
                }
            }
            
            self.pullToRefreshIsEnabled ?= webViewConfig.enablePullToRefresh
            self.shouldShowLoadingOverlay ?= webViewConfig.showLoadingOverlay
            
            let statusBarConfig = webViewConfig.statusBarConfig
            self.statusBarIsHidden ?= statusBarConfig.hidden
            self.statusBarTextStyle ?= statusBarConfig.textStyle
            self.statusBarBackgroundColour ?= statusBarConfig.bgColour
            
            self.inactiveSecondsBeforeRefresh ?= webViewConfig.inactiveSecondsBeforeRefresh
            
            // initiaise the webView
            self.webView = UIWebView()
            self.webView.backgroundColor = UIColor.blackColor()
            
            // add ourselves as the navigation delegate
            self.webView.delegate = self
            
            // set the webview off loading
            self.loadUrl(initialUrl)

            // add the webView to the jsBridge if needed so it can send messages down to the web layer
            if let jsBridge = self.jsBridge {
                jsBridge.webView = self.webView
            }
            
            // set up any js bridge handlers
            self.registerStatusBarJsBridgeHandlers()
            
            self.view.addSubviewAtFullWidthFullHeight(self.webView)
            self.setupPullToRefresh(self.webView)
        } else {
            FodLog("Failed to retrieve WebViewConfig using key \(self.configKey)", .Error)
            super.load(config)
        }
    }
    
    override public func loadFromBackground(interval: NSTimeInterval, config: Config?) {
        if let config = config {
            // we have new config, do a complete reload
            self.load(config)
        } else if interval > self.inactiveSecondsBeforeRefresh {
            // no new config, but we've been out a while, refresh the webview
            self.isInitialLoad = true
            self.reload()
        } else {
            // nothing to do, tell our parent we're ready
            if let readyAction = self.readyAction {
                readyAction(vc: self)
            }
        }
    }

    // MARK: - UI
    
    func showExternalLinkActionSheet(url:NSURL) {
        let alertController = UIAlertController(title: "External link", message: "Would you like to open the link in your web browser?".localized, preferredStyle: .ActionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "Yes", style: .Default) { (action) in
            self.dismissViewControllerAnimated(true, completion: nil)
            
            // proceed to open the link in the system's default browser
            UIApplication.sharedApplication().openURL(url)
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - WebView Control
    
    public func loadUrl(url: NSURL) {
        var request = NSURLRequest(URL: url)
        webView.loadRequest(request)
    }
    
    public func reload() {
        self.webView.reload()
    }
    
    // MARK: - UIWebView Delegate Methods
    
    public func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.URL!
        
        // JS Bridge Handling
        if(self.jsBridge != nil) {
            if url.scheme == JSBridge.Scheme {
                self.jsBridge!.handleUrl(url)
                return false
            }
        }
        
        if let urlValidator = self.urlValidator {
            if(urlValidator.checkUrl(url) == UrlValidator.Result.Internal) {
                return true
            } else {
                showExternalLinkActionSheet(url)
                return false
            }
        }
    
        return true
    }
    
    public func webViewDidStartLoad(webView: UIWebView) {
        self.requestTimer = NSTimer.scheduledTimerWithTimeInterval(self.timeout, target: self, selector: Selector("webViewRequestDidTimeout"), userInfo: nil, repeats: false)
    }
    
    public func webViewDidFinishLoad(webView: UIWebView) {
        self.requestTimer.invalidate()
        
        self.resetPullToRefresh()
        if let jsBridge = self.jsBridge {
            webView.stringByEvaluatingJavaScriptFromString(jsBridge.javascriptCode())
        }
        
        if isInitialLoad {
            if let readyAction = self.readyAction {
                self.isInitialLoad = false
                readyAction(vc: self)
            }
        }
    }
    
    public func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        FodLog("WebView request failed, error: \(error.localizedDescription)", .Error)
        self.requestTimer.invalidate()
        
        if isInitialLoad {
            if let errorAction = self.errorAction {
                errorAction(error: error)
            }
        }
    }
    
    public func webViewRequestDidTimeout() {
        self.webView.stopLoading()
    }

    // MARK: - Pull to refresh

    private func setupPullToRefresh(webView: UIWebView) {
        webView.scrollView.delegate = self
        
        let view = self.refreshIndicator
        view.hidesWhenStopped = false
        view.setTranslatesAutoresizingMaskIntoConstraints(false)
        view.hidden = !self.pullToRefreshIsEnabled
        view.alpha = 0.0
        
        webView.insertSubview(view, belowSubview: webView.scrollView)
        let views = ["spinner": view]
        let hConstraints = [
            NSLayoutConstraint(item: view, attribute: .CenterX, relatedBy: .Equal, toItem: webView, attribute: .CenterX, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: view, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 25.0)
        ]
        let vConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[spinner(25)]", options: NSLayoutFormatOptions(0), metrics: nil, views: views)
        
        webView.addConstraints(hConstraints)
        webView.addConstraints(vConstraints)
        
        self.registerPullToRefreshJsBridgeHandlers()
    }
    
    /// UIScrollView Delegate Method
    public func scrollViewDidScroll(scrollView: UIScrollView) {
        if(!self.pullToRefreshIsEnabled) {
            return
        }
        
        let yOffset = scrollView.contentOffset.y
        
        self.refreshIndicator.alpha = -(yOffset / 65.0)
        
        if(yOffset <= -85.0 && !self.isRefreshing) {
            self.isRefreshing = true
            self.refreshIndicator.startAnimating()
            self.webView.reload()
        } else if (yOffset > -85.0 && self.isRefreshing) {
            scrollView.contentInset.top = 65.0
        }
    }
    
    private func registerPullToRefreshJsBridgeHandlers() {
        self.jsBridge?.registerMessageHandler("enablePullToRefresh") { (data) -> () in
            self.pullToRefreshIsEnabled = true
        }
    
        self.jsBridge?.registerMessageHandler("disablePullToRefresh") { (data) -> () in
            self.pullToRefreshIsEnabled = false
        }
    }
    
    private func resetPullToRefresh() {
        self.isRefreshing = false
        self.refreshIndicator.stopAnimating()
        self.refreshIndicator.alpha = 0.0
        webView.scrollView.contentInset.top = 0.0
    }
    
    // MARK: - JS Bridge
    
    private func registerStatusBarJsBridgeHandlers() {
        self.jsBridge?.registerMessageHandler("hideStatusBar") { (data) -> () in
            self.statusBarIsHidden = true
        }

        self.jsBridge?.registerMessageHandler("showStatusBar") { (data) -> () in
            self.statusBarIsHidden = false
        }

        self.jsBridge?.registerMessageHandler("setStatusBarStyle") { (data) -> () in
            if let styleData = data {
                // TODO: this is repeated logic from StatusBarConfig
                if let hidden = styleData["hidden"] as? Bool {
                    self.statusBarIsHidden = hidden
                }

                if let textStyle = styleData["textStyle"] as? String {
                    switch textStyle {
                    case "light":
                        self.statusBarTextStyle = .LightContent

                    default:
                        self.statusBarTextStyle = .Default
                    }
                }

                if let bgColourString = styleData["bgColour"] as? String {
                    if bgColourString == "none" {
                        self.statusBarBackgroundColour = UIColor.clearColor()
                    } else {
                        self.statusBarBackgroundColour = UIColor(rgbaString: bgColourString)
                    }
                }
                
            }
        }
    }

}