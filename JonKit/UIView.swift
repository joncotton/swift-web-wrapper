//
//  UIView.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 30/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

/**
    Adds the ability to add a new subview at full width and full height by
    automatically adding the relevant auto layout constraints.
*/
extension UIView {
    
    public func addSubviewAtFullWidthFullHeight(subview:UIView) {
        self.addSubview(subview)
        
        subview.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let views = ["subview": subview]
        let hConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[subview]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views)
        let vConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[subview]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views)
        
        self.addConstraints(hConstraints)
        self.addConstraints(vConstraints)
    }
    
}