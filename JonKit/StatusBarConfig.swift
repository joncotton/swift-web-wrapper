//
//  StatusBarConfig.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 27/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

public class StatusBarConfig : ConfigParser {
    
    public struct Key {
        public static let Hidden = "Hidden"
        public static let TextStyle = "TextStyle"
        public static let BgColour = "BgColour"
    }
    
    public var hidden: Bool?
    public var textStyle: UIStatusBarStyle?
    public var bgColour: UIColor?
    
    override public func parse(rawConfig: [String: AnyObject]) {
        super.parse(rawConfig)
        
        var rawTextStyle:String?
        var rawBgColour:String?
        
        self.unpack(Key.Hidden, Into: &self.hidden)
        self.unpack(Key.TextStyle, Into: &rawTextStyle)
        self.unpack(Key.BgColour, Into: &rawBgColour)
        
        if let rawTextStyle = rawTextStyle {
            switch (rawTextStyle) {
            case "light":
                self.textStyle = .LightContent
                
            default:
                self.textStyle = .Default
            }
        }
        
        if let rawBgColour = rawBgColour {
            if rawBgColour == "none" {
                self.bgColour = UIColor.clearColor()
            } else {
                self.bgColour = UIColor(rgbaString: rawBgColour)
            }
        }
    }
}