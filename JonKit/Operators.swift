//
//  Operators.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 17/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

/*
 * += <Dictionary>
 * A bit of Swift magic, adds a += operator for Dictionaries as a shorthand way to merge two Dictionaries (recursive)
 */
func +=<K, V> (inout left: Dictionary<K, V>, right: Dictionary<K, V>) -> Dictionary<K, V> {
    for (k, v) in right {
        if var leftDict = left[k] as? Dictionary<K, V>,
               rightDict = right[k] as? Dictionary<K, V>,
               mergedDict = (leftDict += rightDict) as? V
        {
            left[k] = mergedDict
        } else {
            left[k] = v
        }
    }
    
    return left
}

/**
    ?= Any
    Optional assignment operator, only assigns to the left operand if the right operand is not nil, otherwise left stays as is
*/
infix operator ?= { associativity left precedence 140 }
func ?= <T>(inout left:T, right:T?) -> T {
    if let newLeft = right {
        left = newLeft
    }
    
    return left
}
