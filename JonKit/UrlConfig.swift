//
//  UrlConfig.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 27/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

public class UrlConfig : ConfigParser {
    
    public struct Key {
        public static let InternalUrls = "InternalUrls"
    }
    
    public var internalUrls = ["all": ".*"]
    
    override public func parse(rawConfig: [String: AnyObject]) {
        super.parse(rawConfig)
        
        self.unpack(Key.InternalUrls, Into: &self.internalUrls)
    }
}