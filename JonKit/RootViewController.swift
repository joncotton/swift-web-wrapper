//
//  RootViewController.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 06/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import UIKit

public class RootViewController : BaseViewController {

    public static let SystemStatusBarHeight: CGFloat = 20.0
    public static let ViewTransitionTime: NSTimeInterval = 0.3
    
    private var contentView = UIView()
    private var contentViewTopConstraint: NSLayoutConstraint?
    
    private var statusBarView = UIView()
    
    public var loadingViewController = LoadingViewController()
    public var mainViewController:BaseViewController = WebViewController()
    
    var frontViewController: UIViewController?
    
    // MARK: - View Lifecycle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // setup the content view
        self.setupContentViewConstraints(contentView)
        
        // set up status bar
        self.setupStatusBarView()
    }
    
    // MARK: - Child view controller managment
    
    public func showViewController(vc: UIViewController, animated: Bool) {
        if vc == self.frontViewController {
            // passed vc is already the displayed vc, do nothing
            FodLog("Attempted to show VC that is already displayed", .Notice)
            return
        }
        
        // check if the passed vc is already a child
        if (self.childViewControllers as NSArray).containsObject(vc) {
            FodLog("Bringing View Controller \(vc) to front")
            self.contentView.bringSubviewToFront(vc.view)
        } else {
            // new vc, add as child and add vc.view as subview
            self.addChildViewController(vc)
            self.contentView.addSubviewAtFullWidthFullHeight(vc.view)
            vc.didMoveToParentViewController(self)
            FodLog("Added Child \(vc), Child View Controllers now contains \(self.childViewControllers.count) Items")
        }
        
        if animated {
            if let frontViewController = self.frontViewController {
                UIView.transitionFromView(frontViewController.view,
                    toView:     vc.view,
                    duration:   RootViewController.ViewTransitionTime,
                    options:    .ShowHideTransitionViews | .CurveEaseIn | .TransitionCrossDissolve,
                    completion: nil
                )
            }
        }
        
        self.frontViewController = vc
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    public func showViewController(vc: UIViewController) {
        self.showViewController(vc, animated: true)
    }
    
    public func removeViewController(vc: UIViewController, animated: Bool) {
        // check if the passed vc is a child
        if (self.childViewControllers as NSArray).containsObject(vc) {
            vc.willMoveToParentViewController(nil)
            vc.view.removeFromSuperview()
            vc.removeFromParentViewController()
            FodLog("Removed Child \(vc), Child View Controllers now contains \(self.childViewControllers.count) Items")
        } else {
            FodLog("Attempted to remove non-existent Child VC from Parent", .Notice)
            return
        }
        
        // if we're removing the currently visible vc
        if vc == self.frontViewController {
            if let newFrontViewController = self.childViewControllers.last as? BaseViewController {
                self.showViewController(newFrontViewController, animated: animated)
            } else {
                self.frontViewController = nil
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    public func removeViewController(vc: UIViewController) {
        self.removeViewController(vc, animated: true)
    }
    
    // MARK: - LoadbleViewController methods
    
    override public func load(config: Config) {
        self.loadingViewController.load(config)
        
        // add the mainViewController before loading so any layout constraints are applied before the content loads
        self.showViewController(self.mainViewController, animated: false)
        self.showViewController(self.loadingViewController, animated: false)
        
        mainViewController.readyAction = {[weak self] (vc: UIViewController) -> () in
            self?.showViewController(vc)
        }
        
        mainViewController.errorAction = {[weak self] (error: NSError) -> () in
            // TODO: - This string needs to be cfg'ble and localised
            self?.loadingViewController.showError("Unable to connect to internet, please check your connection and try again") { () -> () in
                self?.mainViewController.load(config)
                return
            }
        }
        
        self.mainViewController.load(config)
    }
    
    override public func loadFromBackground(interval: NSTimeInterval, config: Config?) {
        // show the loading screen then wait for the main VC to fire the readyAction
        self.showViewController(self.loadingViewController)
        self.mainViewController.loadFromBackground(interval, config: config)
    }
    
    // MARK: - Content View
    
    private func setupContentViewConstraints(contentView: UIView) {
        view.addSubview(contentView)
        
        self.contentView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let views = ["contentView": contentView]
        let hConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views)
        self.contentViewTopConstraint = NSLayoutConstraint(item: self.contentView, attribute: .Top, relatedBy: .Equal, toItem: self.view, attribute: .Top, multiplier: 1.0, constant: self.statusBarHeight)
        let vConstraints = [
            self.contentViewTopConstraint!,
            NSLayoutConstraint(item: self.contentView, attribute: .Bottom, relatedBy: .Equal, toItem: self.view, attribute: .Bottom, multiplier: 1.0, constant: 0.0)
        ]
        
        self.view.addConstraints(hConstraints)
        self.view.addConstraints(vConstraints)
    }
    
    // MARK: - Status Bar
    
    private var statusBarHeight: CGFloat {
        // if the status bar is completely hidden
        if(self.prefersStatusBarHidden()) {
            return 0.0
        }
        
        // if there is no status bar background or the status bar bg is not completely opaque
        if let preferredStatusBarBackgroundColour = self.preferredStatusBarBackgroundColour() {
            if preferredStatusBarBackgroundColour.alpha < 1.0 {
                return 0.0
            }
        } else {
            // no background
            return 0.0
        }
        
        // status bar is not hidden and has a fully opaque background colour
        return RootViewController.SystemStatusBarHeight
    }
    
    override public func setNeedsStatusBarAppearanceUpdate() {
        super.setNeedsStatusBarAppearanceUpdate()
        self.updateStatusBarBackgroundView()
    }
    
    override public func prefersStatusBarHidden() -> Bool {
        if let frontViewController = self.frontViewController {
            return frontViewController.prefersStatusBarHidden()
        }
        
        return self.statusBarIsHidden
    }
    
    override public func preferredStatusBarStyle() -> UIStatusBarStyle {
        if let frontViewController = self.frontViewController {
            return frontViewController.preferredStatusBarStyle()
        }
        
        return self.statusBarTextStyle
    }
    
    override public func preferredStatusBarBackgroundColour() -> UIColor? {
        if let frontViewController = self.frontViewController as? BaseViewController {
            return frontViewController.preferredStatusBarBackgroundColour()
        }
        
        return self.statusBarBackgroundColour
    }
    
    public func updateStatusBarBackgroundView() {
        if let frontViewController = self.frontViewController as? BaseViewController {
            if let preferredStatusBarBackgroundColour = frontViewController.preferredStatusBarBackgroundColour() {
                self.statusBarView.hidden = self.prefersStatusBarHidden()
                self.statusBarView.backgroundColor = preferredStatusBarBackgroundColour
                self.statusBarView.alpha = preferredStatusBarBackgroundColour.alpha
            } else {
                self.statusBarView.hidden = true
            }
            
            self.contentViewTopConstraint?.constant = self.statusBarHeight
        }
    }
    
    func setupStatusBarView() {
        self.view.addSubview(self.statusBarView)
        self.statusBarView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        let views = ["statusBarView": self.statusBarView]
        let hConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[statusBarView]|", options: NSLayoutFormatOptions(0), metrics: nil, views: views)
        let vConstraints = [
            NSLayoutConstraint(item: self.statusBarView, attribute: .Top, relatedBy: .Equal, toItem: self.view, attribute: .Top, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self.statusBarView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: RootViewController.SystemStatusBarHeight)
        ]
        
        self.view.addConstraints(hConstraints)
        self.view.addConstraints(vConstraints)
    }

    
}