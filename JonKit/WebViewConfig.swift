//
//  WebViewConfig.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 27/04/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation

public class WebViewConfig : ConfigParser {
    
    public struct Key {
        public static let InitialUrl = "InitialUrl"
        public static let UrlConfig = "UrlConfig"
        public static let EnablePullToRefresh = "EnablePullToRefresh"
        public static let EnableJsBridge = "EnableJsBridge"
        public static let ShowLoadingOverlay = "ShowLoadingOverlay"
        public static let StatusBarConfig = "StatusBarConfig"
        public static let InactiveSecondsBeforeRefresh = "InactiveSecondsBeforeRefresh"
    }
    
    var initialUrl: NSURL?
    var urlConfig = UrlConfig()
    var enablePullToRefresh: Bool?
    var enableJsBridge: Bool?
    var showLoadingOverlay: Bool?
    var statusBarConfig = StatusBarConfig()
    var inactiveSecondsBeforeRefresh: NSTimeInterval?
    var inactiveSecondsBeforeReload: NSTimeInterval?
    
    override public func parse(rawConfig: [String: AnyObject]) {
        super.parse(rawConfig)
        
        var rawInitialUrl:String?
        
        self.unpack(Key.InitialUrl, Into: &rawInitialUrl)
        self.unpack(Key.UrlConfig, Into: &self.urlConfig)
        self.unpack(Key.EnablePullToRefresh, Into: &self.enablePullToRefresh)
        self.unpack(Key.EnableJsBridge, Into: &self.enableJsBridge)
        self.unpack(Key.ShowLoadingOverlay, Into: &self.showLoadingOverlay)
        self.unpack(Key.StatusBarConfig, Into: &self.statusBarConfig)
        self.unpack(Key.InactiveSecondsBeforeRefresh, Into: &self.inactiveSecondsBeforeRefresh)
        
        if let rawInitialUrl = rawInitialUrl {
            self.initialUrl = NSURL(string:rawInitialUrl)
        }
    }
    
}