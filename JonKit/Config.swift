//
//  AppConfig.swift
//  FodAppLibrary
//
//  Created by Jonathan Cotton on 07/05/2015.
//  Copyright (c) 2015 Fleet On Demand. All rights reserved.
//

import Foundation


public class Config : ConfigParser {
    public static let SharedInstance = Config()
    
    /**
        Defined keys for accessing config values, to use:
    
        config.valueForKey(Config.Key.WebViewConfig)
    
        - WebViewConfig     The web view config dictionary
        - UserAgentSuffix   The User Agent string extension
        - RemoteConfigUrl   The location of remote config overrides
    */
    public struct Key {
        public static let LoadingViewConfig                  = "LoadingViewConfig"
        public static let WebViewConfig                      = "WebViewConfig"                        /// The web view config dictionary
        public static let UserAgentSuffix                    = "UserAgentSuffix"                      /// The User Agent string extension
        public static let RemoteConfigUrl                    = "RemoteConfigUrl"                      /// The location of remote config overrides
        public static let InactiveSecondsBeforeConfigRefresh = "InactiveSecondsBeforeConfigRefresh"   /// Number of seconds app needs to be inactive for before triggering a full config reload
        public static let Strings                            = "Strings"
    }
    
    public var loadingViewConfig = LoadingViewConfig()
    public var webViewConfig: [String: WebViewConfig] = Dictionary()
    public var userAgentSuffix: String?
    public var remoteConfigUrl: String?
    public var inactiveSecondsBeforeConfigRefresh: NSTimeInterval?
    public var strings: [String:[String:String]] = Dictionary()
    
    override public func parse(rawConfig:[String:AnyObject]) {
        super.parse(rawConfig)
        
        self.unpack(Key.LoadingViewConfig, Into: &self.loadingViewConfig)
        self.unpack(Key.UserAgentSuffix, Into: &self.userAgentSuffix)
        self.unpack(Key.RemoteConfigUrl, Into: &self.remoteConfigUrl)
        self.unpack(Key.InactiveSecondsBeforeConfigRefresh, Into: &self.inactiveSecondsBeforeConfigRefresh)
        self.unpack(Key.Strings, Into: &self.strings)
        
        var rawWebViewConfigs:[String:[String:AnyObject]]?
        self.unpack(Key.WebViewConfig, Into: &rawWebViewConfigs)
        if let rawWebViewConfigs = rawWebViewConfigs {
            for (key, rawWebViewConfig) in rawWebViewConfigs {
                let webViewConfig = WebViewConfig()
                webViewConfig.parse(rawWebViewConfig)
                self.webViewConfig[key] = webViewConfig
            }
        }
    }
    
}