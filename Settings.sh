#!/bin/sh

#  Settings.sh
#  Copies either the standard Settings bundle or the Debug Settings bundle into the app at build time depending on build config
#
#  FodAppLibrary
#
#  Created by Jonathan Cotton on 28/04/2015.
#  Copyright (c) 2015 Fleet On Demand. All rights reserved.


cp -r "lib/fod-app-library/Settings-${CONFIGURATION}.bundle/" "${BUILT_PRODUCTS_DIR}/${PRODUCT_NAME}.app/Settings.bundle"
