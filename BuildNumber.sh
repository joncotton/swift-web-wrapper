#!/bin/sh

#  BuildNumber.sh
#  FodAppLibrary
#
#  Created by Jonathan Cotton on 30/04/2015.
#  Copyright (c) 2015 Fleet On Demand. All rights reserved.

echo "Bumping build number..."
plist=${XCS_RESOURCE_DIR}/Info.plist

# increment the build number
buildnum=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "${plist}")
if [[ "${buildnum}" == "" ]]; then
echo "No build number in $plist"
exit 2
fi

buildnum=$(expr $buildnum + 1)
/usr/libexec/Plistbuddy -c "Set CFBundleVersion $buildnum" "${plist}"
echo "Bumped build number to $buildnum"

echo "Committing the build number..."
cd ${XCS_PROJECT_DIR};git stage "${plist}"
cd ${XCS_PROJECT_DIR};git commit -m "Bumped the build number."
cd ${XCS_PROJECT_DIR};git push -u origin master
cd ${XCS_PROJECT_DIR};git reset --hard
echo "Build number committed."