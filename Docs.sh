#!/bin/sh

#  Docs.sh
#  FodAppLibrary
#
#  Created by Jonathan Cotton on 30/04/2015.
#  Copyright (c) 2015 Fleet On Demand. All rights reserved.

jazzy -o "/Docs" --source-directory "${XCS_PROJECT_DIR}/lib/fod-app-library"