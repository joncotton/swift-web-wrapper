//
//  FodUrlValidatorTests.swift
//  
//
//  Created by Jonathan Cotton on 14/04/2015.
//
//

import UIKit
import XCTest

import JonKit

class UrlValidatorTests: XCTestCase {
    
    func testCheckUrlReturnsInternalResultWhenUrlMatchesExactly() {
        class MockUrlConfig : UrlConfig {
            override init() {
                super.init()
                internalUrls = ["default":"www\\.test\\.com"]
            }
        }
        
        let mockUrlConfig = MockUrlConfig()
        let urlValidator = UrlValidator(config: mockUrlConfig)
        
        XCTAssertEqual(urlValidator.checkUrl(NSURL(string:"http://www.test.com")!), UrlValidator.Result.Internal, "Expected www.test.com to be in internal url list")

    }
    
    func testCheckUrlReturnsExternalResultWhenUrlDoesNotMatch() {
        class MockUrlConfig : UrlConfig {
            override init() {
                super.init()
                internalUrls = ["nomatch":"www\\.nomatch\\.com\\/.*"]
            }
        }
        
        let mockUrlConfig = MockUrlConfig()
        let urlValidator = UrlValidator(config: mockUrlConfig)
        
        XCTAssertEqual(urlValidator.checkUrl(NSURL(string:"http://www.test.com")!), UrlValidator.Result.NotHandled, "Expected www.test.com not to be in internal url list and return a not handled result")
    }
    
    func testCheckUrlReturnsInternalResultWhenUrlIncludesPathThatMatchesPattern() {
        class MockUrlConfig : UrlConfig {
            override init() {
                super.init()
                internalUrls = ["default":"www\\.test\\.com\\/.*"]
            }
        }
        
        let mockUrlConfig = MockUrlConfig()
        let urlValidator = UrlValidator(config: mockUrlConfig)
        
        XCTAssertEqual(urlValidator.checkUrl(NSURL(string:"http://www.test.com/somepath")!), UrlValidator.Result.Internal, "Expected www.test.com/somepath to be in internal url list")
    }
    
    func testCheckUrlReturnsInternalResultWhenUrlMatchesAnyOfTheInternalUrlPatterns() {
        class MockUrlConfig : UrlConfig {
            override init() {
                super.init()
                internalUrls = ["nomatch1":"www\\.nomatch\\.com", "nomatch2":"www\\.wontmatcheither\\.com", "nomatch3":"www\\.test\\.com\\/non\\/matching\\/path", "match":"www\\.test\\.com\\/.*"]
            }
        }
        
        let mockUrlConfig = MockUrlConfig()
        let urlValidator = UrlValidator(config: mockUrlConfig)
        
        XCTAssertEqual(urlValidator.checkUrl(NSURL(string:"http://www.test.com/somepath/some/more/path")!), UrlValidator.Result.Internal, "Expected http://www.test.com/somepath/extra/path to be in internal url list")
    }

}
