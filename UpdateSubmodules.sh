#!/bin/sh

#  UpdateSubmodules.sh
#  FodAppLibrary
#
#  Created by Jonathan Cotton on 01/05/2015.
#  Copyright (c) 2015 Fleet On Demand. All rights reserved.

cd ${XCS_PROJECT_DIR};git submodule update --init --recursive